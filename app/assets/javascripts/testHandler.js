$(document).ready(function() {
	jQuery.each($("#test_cases").children(), function(index, elem) {
		$(elem).unbind('click').bind('click', function(event) {
			event.preventDefault();
			var elements = $("#elements").val();
			var path = "/run?partial="+this.getAttribute("id")+"&elements="+elements;
			if(window.location && window.location.href) {
				window.location.href = path
			} else if(document.location && document.location.href) {
				document.location.href = path
			} else {
				alert("Browser can't redirect using window.location nor document.location");
			}
		})
	});
});
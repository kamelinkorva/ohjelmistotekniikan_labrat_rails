var testHelper = {
	finished: function() {
		var parameters = testData
		var duration 	= (parameters.duration) ? parameters.duration : null;
		var name 			= (parameters.name) ? parameters.name : null;
		var framework = (parameters.framework) ? parameters.framework : null;
		var device 		= (parameters.device) ? parameters.device : null;
		var browser 	= (parameters.browser) ? parameters.browser : null;
		
		if(duration && name && framework && device && browser) {
			parameters.success = true
		} else {
			parameters.error = "Missing arguments"
		}
		parameters.partial = partial;
		
		params_string = testHelper._constructGetParameters(parameters)
		window.location = "/save?"+params_string;
	},
	
	_constructGetParameters: function(json_object) {
		var params = "";
		for(var key in json_object) {
			params += key+"="+encodeURIComponent(json_object[key])+"&"
		}
		return params.slice(0, -1);
	},
	
	generateColors: function(amount) {
		var colors = [];
		for(var i=0; i<amount; i++) {
			var color = "#";
			for(var j=0; j<6; j++) {
				color += Math.floor(Math.random()*9);
			}
			colors.push(color);
		}
		return colors;
	}
}
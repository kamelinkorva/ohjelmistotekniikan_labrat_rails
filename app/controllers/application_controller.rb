class ApplicationController < ActionController::Base
  protect_from_forgery
  
  before_filter :getDevice
  
  private
  
  def getDevice
    device = params[:device] if params[:device]
    device = session[:device] if (session[:device] and device.nil?)
    unless device.nil?
      session[:device] = device
    end
  end
  
end

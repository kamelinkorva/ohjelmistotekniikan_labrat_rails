class TestResultController < ApplicationController
  
  def saveResult
    success = false;
    
    unless params["error"]
      parameters = {
        :duration => params["duration"],
        :name => params["name"],
        :elements => params["elements"],
        :description => params["description"],
        :framework => params["framework"],
        :device => params["device"],
        :browser => params["browser"]
      }
      result = TestResult.new(parameters)
      success = result.save
    end
    
    if session[:patchResults]
      css_class = "fail"
      css_class = "success" if success
      session[:patchResults][session[:testsToRun]] = {:index => session[:testsToRun], :success => success, :class => css_class, :duration => params["duration"]}
    end
    
    if session[:testsToRun] < 1
      logger.info "Finished test patch, showing results"
      puts "Finished test patch, showing results"
      redirect_to show_patch_result_url, :success => success
    else
      logger.info "Finished test, running next"
      puts "Finished test, running next"
      redirect_to :controller => 'testRunner', :action => 'run', :success => success, :partial => params["partial"]
    end
  end
  
  def showPatchResult
    @patch_results = session[:patchResults]
    duration = 0
    @patch_results.each do |result|
      duration += Integer(result[:duration])
    end
    @average_duration = duration/@patch_results.length
  end
  
  def showResultNumbers
    @insertion_Android = insertion(androidBrowserResults())
    @insertion_iPhone = insertion(iPhoneSafariResults())
    
    @deletion_Android = deletion(androidBrowserResults())
    @deletion_iPhone = deletion(iPhoneSafariResults())
    
    @modify_Android = modify(androidBrowserResults())
    @modify_iPhone = modify(iPhoneSafariResults())
    
    @finding_Android = finding(androidBrowserResults())
    @finding_iPhone = finding(iPhoneSafariResults())
  end
  
  
  private
  
  # TESTS
  
  def insertion(set)
    results = []
    3.times do |i|
      elements = 2000 if i==0
      elements = 4000 if i==1
      elements = 8000 if i==2
      3.times do |j|
        framework = "jQuery" if j==0
        framework = "Mootools" if j==1
        framework = "Prototype" if j==2
        result = set.find(:all, :conditions => {:name => "Insertion", :elements => elements, :framework => framework})
        results.push(resultHelper(result, elements, framework))
      end
    end
    return results
  end
  
  def deletion(set)
    results = []
    3.times do |i|
      elements = 2000 if i==0
      elements = 4000 if i==1
      elements = 8000 if i==2
      3.times do |j|
        framework = "jQuery" if j==0
        framework = "Mootools" if j==1
        framework = "Prototype" if j==2
        result = set.find(:all, :conditions => {:name => "Deletion", :elements => elements, :framework => framework})
        results.push(resultHelper(result, elements, framework))
      end
    end
    return results
  end
  
  def modify(set)
    results = []
    3.times do |i|
      elements = 2000 if i==0
      elements = 4000 if i==1
      elements = 8000 if i==2
      3.times do |j|
        framework = "jQuery" if j==0
        framework = "Mootools" if j==1
        framework = "Prototype" if j==2
        result = set.find(:all, :conditions => {:name => "Modify", :elements => elements, :framework => framework})
        results.push(resultHelper(result, elements, framework))
      end
    end
    return results
  end
  
  def finding(set)
    results = []
    3.times do |i|
      elements = 1000 if i==0
      elements = 1250 if i==1
      elements = 1500 if i==2
      3.times do |j|
        framework = "jQuery" if j==0
        framework = "Mootools" if j==1
        framework = "Prototype" if j==2
        result = set.find(:all, :conditions => {:name => "Finding", :elements => elements, :framework => framework})
        results.push(resultHelper(result, elements, framework))
      end
    end
    return results
  end
  
  # DEVICES
  
  def androidBrowserResults
    return TestResult.where('browser LIKE ? AND device LIKE ?', '%Safari%', '%Android%')
  end
  
  def iPhoneSafariResults
    return TestResult.where('browser LIKE ? AND device LIKE ?', '%Safari%', '%iPhone%')
  end
  
  def androidOperaResults
    return TestResult.where('browser LIKE ? AND device LIKE ?', '%Opera%', '%Android%')
  end
  
  def iPhoneOperaResults
    return TestResult.where('browser LIKE ? AND device LIKE ?', '%Opera%', '%iPhone%')
  end
  
  def n95Results
    return TestResult.where('device LIKE ?', '%N95%')
  end
  
  def resultHelper (result, elements, framework)
    durations = []
    result.select { |res| durations.push(res['duration']) }
    
    deviation = durations.standard_deviation
    average = durations.average
    
    offliers = result.select { |res| ((res['duration']-average).abs > 3*deviation) }
    offlier_durations = []
    good_results = result - offliers
    good_results.delete(offliers)
    good_results.select { |res| offlier_durations.push(res['duration']) }
    
    obj = {"framework" => framework, "elements" => elements, "results" => result.length, "deviation" => deviation, "average" => average, "offliers" => offliers, "offlier_average" => offlier_durations.average}
    return obj
  end
  
end


module Enumerable

    def sum
      self.inject(0){|accum, i| accum + i }
    end

    def mean
      self.sum/self.length.to_f
    end

    def sample_variance
      m = self.mean
      sum = self.inject(0){|accum, i| accum +(i-m)**2 }
      sum/(self.length - 1).to_f
    end

    def standard_deviation
      return Math.sqrt(self.sample_variance)
    end
    
    def average
      return self.inject(0.0) { |sum, el| sum + el } / self.size
    end

end

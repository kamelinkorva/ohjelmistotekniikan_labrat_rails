class TestRunnerController < ApplicationController
  
  def selectDevice

  end
  
  def displayTests
    logger.info "Displaying tests and setting test num to 50"
    puts "Displaying tests and setting test num to 50"
    @last_was_success = nil
    unless params["success"].nil?
      @last_was_success = params["success"]
    end
    session[:testsToRun] = 50
    session[:patchResults] = Array.new
  end
  
  def run
    logger.info "Ran a test"
    puts "Ran a test"
    @partial_name = params["partial"]
    logger.debug "param elements: " << params["elements"].to_s
    logger.debug "condition: " << (session[:numberOfElements].nil? or params["elements"].to_i != session[:numberOfElements]).to_s
    if (session[:numberOfElements].nil? or (!params["elements"].nil? and params["elements"].to_i != session[:numberOfElements]))
      session[:numberOfElements] = params["elements"].to_i
    end
    @numberOfElements = session[:numberOfElements]
    session[:testsToRun] -= 1
  end
  
end

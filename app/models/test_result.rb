class TestResult < ActiveRecord::Base
  
  validates_presence_of :duration, :name, :executed, :framework, :device, :browser
  before_validation :setExecutedTime
  
  def setExecutedTime
    self.executed = DateTime.current()
  end
  
end

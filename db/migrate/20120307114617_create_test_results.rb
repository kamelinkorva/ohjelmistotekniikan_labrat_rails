class CreateTestResults < ActiveRecord::Migration
  def change
    create_table :test_results do |t|
      t.integer :duration
      t.string :name
      t.integer :elements
      t.string :description
      t.datetime :executed
      t.string :framework
      t.string :device
      t.string :browser

      t.timestamps
    end
  end
end
